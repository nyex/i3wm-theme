#!/bin/bash

# =============================================================
# CONFIGURACOES DO SCRIPT
# =============================================================

CONF_WALLPAPER = wallapaper_01.jpg

# =============================================================
# INSTALACOES
# =============================================================

# Preparando ambiente do PACMAN
sudo sed -i '/^\s*#Color/s/^#//' /etc/pacman.conf
sudo sed -i 's/^#*\(ParallelDownloads *= *\).*/\115/' /etc/pacman.conf

# Atualizando sistema
sudo pacman -Syu --noconfirm

# Instalando programas base
sudo pacman -S git base-devel kitty --needed --noconfirm

# Instalando requisitos do tema
sudo pacman -S feh picom rofi alsa-utils --needed --noconfirm

# Instalando requisitos do i3blocks
sudo pacman -S python-dbus gnome-calendar gnome-weather --needed --noconfirm

# Instalando requisitos do terminal
sudo pacman -S zsh zsh-completions --needed --noconfirm

# Instalando temas graficos
# sudo pacman -S materia-gtk-theme papirus-icon-theme lxappearance --needed --noconfirm

# Instalando fontes
sudo pacman -S ttf-font-awesome ttf-ubuntu-font-family ttf-jetbrains-mono-nerd --needed --noconfirm

# Adicionais
sudo pacman -S neofetch --needed --noconfirm

# Instalando Discord
sudo pacman -S discord --needed --noconfirm

# Instalando Remmina
sudo pacman -S remmina freerdp --needed --noconfirm

# =============================================================
# INSTALACOES - AUR
# =============================================================

# Preparando ambiente AUR
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si
cd ../
rm -rf ./yay

# Editor de texto
yay -S --noconfirm visual-studio-code-bin

# Thema do terminal - ZSH
yay -S --noconfirm zsh-theme-powerlevel10k-git

# Streming de musica
yay -S --noconfirm spotify-launcher

# Navegador
yay -S --noconfirm google-chrome

# Gerenciador de senha


# Movendo dados
cp ./imgs/$CONF_WALLPAPER ~/.config/wallpaper.jpg
cp ./config/config ~/.config/i3/config
cp ./config/config.i3block ~/.config/i3/config.block
cp ./config/config.i3status ~/.config/i3/config.status
cp ./apps/picom.conf ~/.config/picom.conf
cp ./apps/kitty.conf ~/.config/kitty/kitty.conf


# Configurando ZSH
echo 'source /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme' >>~/.zshrc
chsh -s $(which zsh)

i3-msg restart