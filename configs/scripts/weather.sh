#!/bin/bash

curl v2.wttr.in/Florianopolis > ./weather.result

ls weather.result > /dev/null
if [ $? -ne 0 ]; then
  echo "⟳ Prev Tempo"
else
  echo $(cat weather.result | grep Weather | tail -n 1 | awk '/Weather/ { print $2, $5, $7 }' | tr ',' ' ')
fi
rm ./weather.result 2> /dev/null