#!/bin/bash

# Fonte: https://github.com/kiddico/i3_config/blob/master/blocks/clock.sh

case $BLOCK_BUTTON in
1) ;; # left   click
2) ;; # middle click
3) ;; # right  click
4) ;; # scroll up
5) ;; # scroll down
esac

echo " $(date '+%R')"