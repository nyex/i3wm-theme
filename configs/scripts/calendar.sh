#!/bin/bash

# Fonte: https://github.com/kiddico/i3_config/blob/master/blocks/calendar.sh

echo " $(date +%y‧%-m‧%-d)"
#echo " $(date +%D)"

case $BLOCK_BUTTON in
	1) gnome-calendar;; # l click
	2) ;; # middle clock click
	3) ;; # right click, mute/unmute
	4) ;; # scroll up, increase
	5) ;; # scroll down, decrease
esac